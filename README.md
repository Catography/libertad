# The Libertad Platform

## How Libertad Works
Johnny the factory worker is told by his greedy boss that, in order to fund the boss's private jet, Johnny's christmas bonus is gonna be cut, and as a result, Johnny's daughter won't be able to afford her cancer medicine. Johnny can't just call up the local newspaper because his boss owns it, so instead he downloads the Tor Browser, loads up the Libertad Hidden Service, creates an account, and makes a post. The international community riots until Johnny's boss steps down and his daughter gets her treatment.

## How Libertad (actually works)
Libertad is written in Python using the Django web framework. There are two basic models:
 - A user
   - A user account created in `/new_user`
 - A post
   - A model with a title and a some content. This is where Johnny describes why his daughter can't get cancer treatment.

## Running Libertad
 - `python3 manage.py runserver`
    - Make sure you run it with the proper IP configuration so that it can only be accessed over the dark web.

## File structure:

 - `libertad`
    - `settings.py`
       - Just some settings for the web server
    - `urls.py`
       - All of the routes that it will serve
    - `wsgi.py`
       - Random auto-generated stuff that we don't need to worry about
       - Tell you what, after this point, if something isn't relevant, then I'll just not have a note about it.
 - `libertad_app`
    - `admin.py`
    - `apps.py`
    - `forms.py`
       - This is where we store Django forms that are needed for user account creation and making posts.
    - `models.py`
 - `templates/`
    - Actual HTML templates that the server will serve

## Dependencies:
I put together a neat little shell script to install all the dependencies for you. Don't worry, you can thank me later. It's called `install_dependencies.sh`, so you might have a little trouble pointing it out.

## How to deploy Libertad

First, get Libertad runnning as a Linux service. The tutorial here is a pretty good starter:
 - https://medium.com/@benmorel/creating-a-linux-service-with-systemd-611b5c8b91d6

## How to turn Libertad into a Tor Hidden Service

Follow the instructions here:
 - https://2019.www.torproject.org/docs/tor-onion-service.html.en
