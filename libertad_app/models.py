from django.db import models

# Create your models here.

class PostManager(models.Manager):
    def create_post(self, name, text, owner):
        resource = self.create(name=name, text=text, owner=owner)

        return resource


class Post(models.Model):
    name = models.CharField(max_length=150)
    owner = models.CharField(max_length=150)
    text = models.CharField(max_length=5000)

    objects = PostManager()
