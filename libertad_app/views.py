from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from .forms import CreateAccountForm, NewPostForm

from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login

from .models import Post

# Create your views here.
def new_user(request):
    if(request.method == 'POST'):
        create_form = CreateAccountForm(request.POST)
        if create_form.is_valid():
            # User is creating a new user, and all form elements are present.
            username = create_form.cleaned_data['create_name']
            password = create_form.cleaned_data['create_pswd']
            user = User.objects.create_user(username=username,
                         password=password)

            return HttpResponseRedirect('/user/' + username + '/')

    else:
        create_form = CreateAccountForm()
        return render(request, 'new_user.html', {'create_form': create_form})

def view_user(request, username):
    user = User.objects.filter(username=username)[0]

    return render(request, 'user.html', {'user': user})

def view_post(request, post_name):
    post = Post.objects.filter(name=post_name)[0]

    return render(request, 'post.html', {'post_name': post.name, 'post_text': post.text})

def view_all_posts(request):
    posts = Post.objects.all()

    posts = [{'name': p.name, 'text':p.text} for p in posts]

    return render(request, 'all_posts.html', {'posts': posts})

def render_new_post(request, username):
    if(request.method == "POST"):
        # Creating a new resource
        form = NewPostForm(request.POST)
        if(form.is_valid()):
            # Create a new Resource
            rcs_name = form.cleaned_data['create_name']
            rcs_text = form.cleaned_data['create_text']

            post = Post.objects.create_post(rcs_name, rcs_text, username)

        return HttpResponseRedirect('/post/' + rcs_name + "/")
    else:
        new_resource_form = NewPostForm()

        return render(request, 'new_post.html', {'create_form': new_resource_form})
