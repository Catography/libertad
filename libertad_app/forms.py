from django import forms

class CreateAccountForm(forms.Form):
    create_name = forms.CharField(label='Username', max_length=100,
                                widget=forms.TextInput(attrs={'class':       'text-input',
                                                              'size':        '50%'}))
    create_pswd = forms.CharField(label='Password', max_length=100,
                                widget=forms.TextInput(attrs={'class':       'text-input',
                                                              'size':        '50%'}))
class NewPostForm(forms.Form):
    create_name = forms.CharField(label='Post Name', max_length=100,
                                widget=forms.TextInput(attrs={'class':       'text-input',
                                                              'size':        '50%'}))
    create_text = forms.CharField(label='Text', max_length=5000,
                                widget=forms.TextInput(attrs={'class':       'text-input',
                                                              'size':        '50%'}))
