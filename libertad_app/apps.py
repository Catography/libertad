from django.apps import AppConfig


class LibertadAppConfig(AppConfig):
    name = 'libertad_app'
